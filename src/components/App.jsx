import React, { useState, useEffect } from "react";

import { AppContainer } from "assets/styles/App.styles.jsx";

import { getScreenshots } from "actions/screenshots.js";

const App = () => {
  const [screenshots, setScreenshots] = useState({});

  useEffect(() => {
    getScreenshots().then((s) => setScreenshots(s));
  }, []);

  return (
    <AppContainer>
      {Object.values(screenshots).map((screenshot, i) => (
        <img key={i} src={screenshot} />
      ))}
    </AppContainer>
  );
};

export default App;
