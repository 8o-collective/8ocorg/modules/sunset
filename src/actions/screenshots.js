import { Communicator } from "@8ocorg/communicator";

const subdomains = [
  // "codex",
  // "etudes",
  // "glyphs",
  "redlog",
  "registry",
  // "titro",
  // "warden",
  // "y8k",
];

const getScreenshots = () => {
  let comm = new Communicator(subdomains);
  return Promise.all(comm.frames.map((frame) => frame.get("screenshot")));
};

export { getScreenshots };
